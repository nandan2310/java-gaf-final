**google-java-format-git-pre-commit-hook**

A git pre-commit hook script for formatting the Java source files according to Google Java Style Guide

Place the pre-commit script (with that name and executable bit set) in your .git/hooks directory.

```
chmod +X .git/hooks/pre-commit

```

The script will download (and cache) the Jar from https://github.com/google/google-java-format and try to re-format all changed .java files in the repository. If any re-formatting did occur, the commit will be aborted, so that you can incorporate formatting fixes.


After this whenever developer try to commit code in their local repo it will verify code at same time due to pre-commit.
